from database import get_db
from fastapi import APIRouter, Depends, status
from fastapi.exceptions import HTTPException
from models import User
from registration.schemas import CreateUserSchema
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from utils import hash_password

reg_router = APIRouter(
    prefix="/registration",
    tags=["Registration"],
)


@reg_router.post("/", status_code=status.HTTP_201_CREATED)
async def get_operations(payload: CreateUserSchema, db: Session = Depends(get_db)):
    user = db.query(User).filter(User.email == payload.email.lower()).first()
    if user:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="Account already exists")

    new_user = User(
        email=payload.email.lower(),
        username=payload.username,
        password=hash_password(payload.password),
        role=payload.role.lower(),
    )
    db.add(new_user)
    try:
        db.commit()
    except IntegrityError:
        db.rollback()
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Database error while adding user"
        )

    return {"data": user, "detail": f"User {new_user.email} successfully has been registered."}
