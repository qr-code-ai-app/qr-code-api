from typing import Any

from pydantic import BaseModel, EmailStr, constr


class UserBaseSchema(BaseModel):
    email: EmailStr
    username: str


class CreateUserSchema(UserBaseSchema):
    password: constr(min_length=8)
    role: str = "user"


class DefaultResponseModel(BaseModel):
    status: str
    data: Any
    details: str


# class UserInDB(User):
#     hashed_password: str
