from auth.router import auth_router
from database import init_db
from fastapi import FastAPI
from qrcode_generation.router import generation_router
from registration.router import reg_router
from admin.router import admin_router
from fastapi.middleware.cors import CORSMiddleware

init_db()
app = FastAPI(title="QR-Code Generation")
app.include_router(reg_router)
app.include_router(auth_router)
app.include_router(generation_router)
app.include_router(admin_router)

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "HEAD", "OPTIONS"],
    allow_headers=[
        "Access-Control-Allow-Headers",
        "Content-Type",
        "Authorization",
        "Access-Control-Allow-Origin",
        "Set-Cookie",
        "Allow-Origin-With-Credentials",
        "Access-Control-Allow-Credentials",
    ],
)
