from gradio_client import Client
from models import User
from qrcode_generation.crud import increase_usage_count
from sqlalchemy.orm import Session

gradio_api = Client("https://huggingface-projects-qr-code-ai-art-generator.hf.space/")


async def get_one_try_qr_code(user: User, url: str, prompt: str, db: Session) -> str:
    path_to_qr_code = await get_qr_code(url, prompt)
    await increase_usage_count(user, db)

    return path_to_qr_code


async def get_qr_code(url: str, prompt: str) -> str:
    result = gradio_api.predict(
        url,
        prompt,
        "ugly, disfigured, low quality, blurry, nsfw",
        7.5,
        1.5,
        0.9,
        2523992465,
        "",
        "",
        True,
        "DPM++ Karras SDE",
        fn_index=0,
    )
    return result
