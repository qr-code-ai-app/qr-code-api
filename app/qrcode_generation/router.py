from fastapi import APIRouter, Request, status, Depends
from fastapi.responses import FileResponse
from qrcode_generation.services import get_one_try_qr_code
from services import get_current_user
from sqlalchemy.orm import Session
from database import get_db

generation_router = APIRouter(
    prefix="/qr-code",
    tags=["QR-Code"],
    responses={404: {"description": "Not found"}},
)


@generation_router.get("/generate", status_code=status.HTTP_200_OK)
async def authenticate_user(request: Request, url: str, prompt: str, db: Session = Depends(get_db)):
    token = request.cookies.get("access_token", None)
    user = await get_current_user(token)

    path_to_qr_code = await get_one_try_qr_code(user, url, prompt, db)
    response = FileResponse(path_to_qr_code)

    return response
