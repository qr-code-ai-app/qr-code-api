from models import User
from sqlalchemy.orm import Session
from auth.utils import _check_is_user_active


async def increase_usage_count(
    user: User,
    db: Session,
):
    current_user = db.query(User).where(User.username == user.username).first()

    _check_is_user_active(user)

    current_user.usage_count += 1
    db.commit()
