from pydantic import BaseModel, EmailStr, constr


class LoginUserSchema(BaseModel):
    email: EmailStr
    password: constr(min_length=8)


class UserResponse(BaseModel):
    email: EmailStr
    username: str


class TokenModel(BaseModel):
    token: str
    token_type: str = "Bearer"
    expires_in: int  # in seconds


class TokenResponse(BaseModel):
    access_token: str
    refresh_token: str
    token_type: str = "Bearer"
    expires_in_access: int  # in seconds
    expires_in_refresh: int  # in seconds
