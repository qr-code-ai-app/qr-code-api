import os

from pydantic import BaseModel


class JWTSettings(BaseModel):
    JWT_ALGORITHM: str = os.getenv("JWT_ALGORITHM")
    JWT_SECRET: str = os.getenv("JWT_SECRET")
    ACCESS_TOKEN_EXPIRE_MIN: int = int(os.getenv("ACCESS_TOKEN_EXPIRE_MIN"))
    REFRESH_TOKEN_EXPIRE_MIN: int = int(os.getenv("REFRESH_TOKEN_EXPIRE_MIN"))
