from auth.config import JWTSettings
from auth.schemas import TokenResponse, TokenModel
from auth.utils import (
    _check_is_user_active,
    _check_is_user_in_db,
    _check_user_password,
    _get_new_user_token,
    get_token_payload,
    _create_jwt_token,
)
from fastapi import status
from fastapi.exceptions import HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from models import User
from redis import Redis
from sqlalchemy.orm import Session
from datetime import timedelta

settings = JWTSettings()


async def get_new_token(data: OAuth2PasswordRequestForm, db: Session) -> TokenResponse:
    user: User = db.query(User).filter(User.email == data.email).first()

    _check_is_user_in_db(user)
    _check_is_user_active(user)
    _check_user_password(data.password, user.password)

    return await _get_new_user_token(user)


async def get_refreshed_token(refresh_token: str, db: Session, redis: Redis) -> TokenModel:
    if not refresh_token:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid Refresh Token.",
        )
    await check_refresh_token_in_blocklist(refresh_token, redis)
    payload = await get_token_payload(token=refresh_token)
    if not payload:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid Refresh Token.",
            headers={"WWW-Authenticate": "Bearer"},
        )
    email = payload.get("email", None)
    if not email:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid Refresh Token.",
            headers={"WWW-Authenticate": "Bearer"},
        )

    user = db.query(User).filter(User.email == email).first()

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="The user belonging to this token no logger exist",
            headers={"WWW-Authenticate": "Bearer"},
        )

    token_expiry = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MIN)
    new_access_token = await _create_jwt_token(user, token_expiry)
    token = TokenModel(token=new_access_token, expires_in=token_expiry.seconds)

    return token


async def check_refresh_token_in_blocklist(token: str, redis: Redis):
    value = redis.get(token)

    if value:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Refresh Token was expired.",
            headers={"WWW-Authenticate": "Bearer"},
        )


async def add_refresh_token_to_blocklist(token: str, expire_in: int, redis: Redis):
    redis.set(token, "block", expire_in)


# async def verify_access_token(token: str):
#     user = await get_current_user(token)
#     if not user:
#         raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid Access Token")
