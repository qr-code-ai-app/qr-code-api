from auth.database import get_redis_session
from auth.services import add_refresh_token_to_blocklist, get_new_token
from database import get_db
from fastapi import APIRouter, Depends, status, Cookie
from fastapi.responses import JSONResponse
from redis import Redis
from services import get_current_user, get_response_with_access_token
from sqlalchemy.orm import Session
from auth.utils import settings
from auth.services import get_refreshed_token
from auth.schemas import LoginUserSchema
from typing import Annotated

auth_router = APIRouter(
    prefix="/auth",
    tags=["Authentication"],
    responses={404: {"description": "Not found"}},
)


@auth_router.post("/login", status_code=status.HTTP_200_OK)
async def authenticate_user(payload: LoginUserSchema, db: Session = Depends(get_db)):
    token = await get_new_token(payload, db)

    response = JSONResponse(content=token.model_dump())
    response.set_cookie(key="access_token", value=token.access_token, expires=token.expires_in_access)
    response.set_cookie(key="refresh_token", value=token.refresh_token, expires=token.expires_in_refresh, path="/auth")
    response.headers.append("Access-Control-Allow-Origin", "*")

    return response


@auth_router.get("/refresh", status_code=status.HTTP_200_OK)
async def refresh_access_token(
    refresh_token: Annotated[str | None, Cookie()] = None,
    db: Session = Depends(get_db),
    redis: Redis = Depends(get_redis_session),
):
    token = await get_refreshed_token(refresh_token, db, redis)
    response = await get_response_with_access_token({"status": "success"}, token.token, token.expires_in)
    return response


@auth_router.get("/me", status_code=status.HTTP_200_OK)
async def get_user_detail(access_token: Annotated[str | None, Cookie()] = None):
    user = await get_current_user(access_token)
    response = JSONResponse(content={"username": user.username, "role": user.role})
    return response


@auth_router.post("/logout", status_code=status.HTTP_200_OK)
async def user_logout(
    refresh_token: Annotated[str | None, Cookie()] = None,
    redis: Redis = Depends(get_redis_session),
):
    # if refresh_token:
    await add_refresh_token_to_blocklist(refresh_token, settings.REFRESH_TOKEN_EXPIRE_MIN, redis)

    response = JSONResponse(content={"status": "success"})
    response.delete_cookie("refresh_token", path="/auth")
    response.delete_cookie("access_token")

    return response
