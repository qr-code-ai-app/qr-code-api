import os

import redis


def get_redis_session():
    redisCli = redis.Redis(
        host="cache",
        port=os.getenv("REDIS_PORT"),
        charset="utf-8",
        decode_responses=True,
        password=os.getenv("REDIS_PASSWORD"),
    )
    try:
        yield redisCli
    finally:
        redisCli.close()
