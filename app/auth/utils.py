from datetime import datetime, timedelta

from auth.config import JWTSettings
from fastapi import status
from fastapi.exceptions import HTTPException
from jose import JWTError, jwt
from models import User
from utils import verify_password
from auth.schemas import TokenResponse

settings = JWTSettings()


async def _create_jwt_token(user: User, expire_in: timedelta):
    payload = {"email": user.email}
    access_token = await create_token(payload, expire_in)
    return access_token


async def _get_new_user_token(user: User) -> TokenResponse:
    access_token_expiry = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MIN)
    refresh_token_expiry = timedelta(minutes=settings.REFRESH_TOKEN_EXPIRE_MIN)
    access_token = await _create_jwt_token(user, access_token_expiry)
    refresh_token = await _create_jwt_token(user, refresh_token_expiry)

    return TokenResponse(
        access_token=access_token,
        refresh_token=refresh_token,
        expires_in_access=access_token_expiry.seconds,  # in seconds
        expires_in_refresh=refresh_token_expiry.seconds,
    )


async def create_token(data, expiry: timedelta):
    payload = data.copy()
    expire_in = datetime.utcnow() + expiry
    payload.update({"exp": expire_in})
    return jwt.encode(payload, settings.JWT_SECRET, algorithm=settings.JWT_ALGORITHM)


async def get_token_payload(token):
    try:
        payload = jwt.decode(token, settings.JWT_SECRET, algorithms=[settings.JWT_ALGORITHM])
    except JWTError:
        return None
    return payload


def _check_is_user_in_db(user: User):
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect Username or Password",
            headers={"WWW-Authenticate": "Bearer"},
        )


def _check_is_user_active(user: User):
    if not user.is_active:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Your account is inactive. Please contact support.",
            headers={"WWW-Authenticate": "Bearer"},
        )


def _check_user_password(payload_password: str, user_password: str):
    if not verify_password(payload_password, user_password):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect Username or Password",
            headers={"WWW-Authenticate": "Bearer"},
        )
