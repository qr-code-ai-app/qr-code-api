from models import User
from fastapi import HTTPException, status


def _check_user_role(user: User):
    if user.role != "admin":
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Permission denied. Your role is not admin.",
        )
