from fastapi import APIRouter, Request, status, Depends, Cookie
from services import get_current_user
from sqlalchemy.orm import Session
from database import get_db
from admin.crud import get_all_users, toggle_user_activity
from admin.utils import _check_user_role
from typing import Annotated

admin_router = APIRouter(
    prefix="/admin",
    tags=["Admin"],
    responses={404: {"description": "Not found"}},
)


@admin_router.get("/get_all_users", status_code=status.HTTP_200_OK)  # response_model=List[UserRepresentation])
async def all_users(request: Request, db: Session = Depends(get_db)):
    token = request.cookies.get("access_token", None)
    user = await get_current_user(token)
    _check_user_role(user)

    users = await get_all_users(db)

    return users


@admin_router.post("/change_user_activity", status_code=status.HTTP_200_OK)
async def user_activity_change(
    username: str,
    access: bool,
    db: Session = Depends(get_db),
    access_token: Annotated[str | None, Cookie()] = None,
):
    user = await get_current_user(access_token)
    _check_user_role(user)

    toggle_user_activity(username, access, db)

    return (
        {
            "data": {"username": username, "active": access},
            "detail": "User successfully blocked.",
        },
    )
