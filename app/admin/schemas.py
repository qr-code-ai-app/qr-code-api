from pydantic import BaseModel, EmailStr


class UserRepresentation(BaseModel):
    email: EmailStr
    username: str
    usage_count: int
