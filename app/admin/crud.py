from sqlalchemy.orm import Session
from models import User


async def get_all_users(db: Session):
    users = db.query(User).all()
    users = [
        {
            "username": user.username,
            "email": user.email,
            "service_usage": user.usage_count,
            "is_active": user.is_active,
        }
        for user in users
    ]
    return users


def toggle_user_activity(username: str, active: bool, db: Session):
    user_to_block = db.query(User).where(User.username == username).first()
    user_to_block.is_active = active
    db.commit()
