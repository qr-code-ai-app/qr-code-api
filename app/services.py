from auth.utils import get_token_payload
from database import get_db
from fastapi import Depends
from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordBearer
from models import User
from fastapi.exceptions import HTTPException
from fastapi import status

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/token")


async def get_current_user(token: str = Depends(oauth2_scheme), db=None) -> User:
    if not token:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid Access Token.",
        )
    payload = await get_token_payload(token)
    if not payload or type(payload) is not dict:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid Access Token")

    email = payload.get("email", None)
    if not email:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid Access Token")

    if not db:
        db = next(get_db())

    user = db.query(User).filter(User.email == email).first()
    return user


# async def get_user_tokens(request: Request) -> TokenModel:
#     access_token = request.cookies.get("access_token", None)
#     refresh_token = request.cookies.get("refresh_token", None)

#     if not access_token:  # and not refresh_token:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="Invalid Access Token.",
#             headers={"WWW-Authenticate": "Bearer"},
#         )

#     # if not refresh_token:
#     #     raise HTTPException(
#     #         status_code=status.HTTP_401_UNAUTHORIZED,
#     #         detail="Invalid Refresh Token.",
#     #         headers={"WWW-Authenticate": "Bearer"},
#     #     )

#     # token = await get_refreshed_token(refresh_token=refresh_token, db=next(get_db()), redis=next(get_redis_ses
# sion()))

#     return access_token


async def get_response_with_access_token(content: dict, access_token: str, expire_in: int) -> JSONResponse:
    response = JSONResponse(content=content)
    response.set_cookie("access_token", access_token, expires=expire_in)

    return response
