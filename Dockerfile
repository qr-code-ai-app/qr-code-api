FROM python:3.11-slim

ENV POETRY_VERSION=1.7.1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    TZ=UTC 

RUN pip install poetry==${POETRY_VERSION}
RUN mkdir -p /app
COPY pyproject.toml ./
RUN poetry install

COPY ./app /app
WORKDIR /app

EXPOSE 80

CMD ["poetry", "run", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
